package SetCollection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MainSet {
    public static void main(String[] args) {
        Set<String> names = new HashSet<>();
        names.add("Reza");
        names.add("Irvan");
        names.addAll(List.of("Irpan","Pahreja", "Patima", "Tertia"));

        System.out.println(names);

        Set<String> hobbies = new LinkedHashSet<>();
        hobbies.add("Games");
        hobbies.add("E-Sport");
        hobbies.add("E-Learning");
        hobbies.add("E-Shopping");

        System.out.println(hobbies);
    }
}

