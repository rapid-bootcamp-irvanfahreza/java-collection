package listCollection;

import model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainList {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>(Arrays.asList("Irvin", "Fahreza","Fatimi","Tertia"));
        System.out.println(names);

        names.set(0, "Irvan");
        names.set(2, "Fatima");
        System.out.println(names);

        List<Person> person = new ArrayList<>(
                Arrays.asList(
                        new Person(1,"Reza", "Jaksel"),
                        new Person(2,"Azer", "Jakbar"),
                        new Person(3, "Fahre","Jakut")
                )
        );

        for (int i = 0; i < person.size(); i++) {
            System.out.println("Index ke:"+i+": "+person.get(i));
        }

        System.out.println("\nSetelah diubah: ");
        person.set(1, new Person(2, "Achmad", "Palembang"));
        for (int i = 0; i < person.size(); i++) {
            System.out.println("Index ke "+i+": "+person.get(i));
        }
    }
}
